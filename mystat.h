#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/sysmacros.h>
#include <limits.h>
#include <string.h>
#include <pwd.h>
#include <errno.h>
#include <grp.h>
#include <time.h>

void printPerms(mode_t m) // function to print the permissions of each files and their octal value
{
	char userbits[4] = "000\0";
	char grpbits[4] = "000\0";
	char othbits[4] = "000\0";

	// stuff borrowed from man pages
	// checking file type
	if (S_ISREG(m))
		printf("-");
	else if (S_ISDIR(m))
		printf("d");
	else if (S_ISCHR(m))
		printf("c");
	else if (S_ISBLK(m))
		printf("b");
	else if(S_ISFIFO(m))
		printf("p");
	else if (S_ISLNK(m))
		printf("l");
	else if (S_ISSOCK(m))
		printf("s");

	// Could not figure out how to print mode in rwx format.
	// Looked up online and found this: https://stackoverflow.com/questions/26616038/how-do-i-print-file-permissions-as-a-string
	// Credits to user "David Winkler". The code below was adapted from his solution. 

	// setting the bits and printing permissions.
	(m & S_IRUSR) ? (printf("r")), userbits[0] = '1' : (printf("-"));
	(m & S_IWUSR) ? (printf("w")), userbits[1] = '1' : (printf("-"));
	(m & S_IXUSR) ? (printf("x")), userbits[2] = '1' : (printf("-"));
	userbits[3] = '\0';

	(m & S_IRGRP) ? (printf("r")), grpbits[0] = '1' : (printf("-"));
	(m & S_IWGRP) ? (printf("w")), grpbits[1] = '1' : (printf("-"));
	(m & S_IXGRP) ? (printf("x")), grpbits[2] = '1' : (printf("-"));
	grpbits[3] = '\0';


	(m & S_IROTH) ? (printf("r")), othbits[0] = '1' : (printf("-"));
	(m & S_IWOTH) ? (printf("w")), othbits[1] = '1' : (printf("-"));
	(m & S_IXOTH) ? (printf("x")), othbits[2] = '1' : (printf("-"));
	othbits[3] = '\0';

	long user = strtol(userbits, NULL, 2);
	long grp = strtol(grpbits, NULL, 2);
	long oth = strtol(othbits, NULL, 2);
	printf("        (%lo%lo%lo in octal)", user, grp, oth);
	printf("\n");
	//printf("USR: %s GRP: %s OTH: %s\n", userbits, grpbits, othbits);
}

void printSymLink(char * arg)
{
	// Portions of code was borrowed form the man page 2 of stat
	ssize_t bufsiz;
	ssize_t nbytes;
	char * buf = NULL;
	
	bufsiz = PATH_MAX;

	buf = malloc(bufsiz);
	if (buf == NULL) // malloc failed somehow...
	{
		perror("readlink buf is null\n");
		exit(EXIT_FAILURE);
	}

	nbytes = readlink(arg, buf, bufsiz);
	if (nbytes == -1)
	{
		perror("readlink failed\n");
		exit(EXIT_FAILURE);
	}

	buf[nbytes] = '\0';

	if (strcmp(buf, "DOES_NOT_EXIST") == 0) // customizing this msg
		printf("- with dangling destination\n");
	else
		printf("-> %s\n", buf);

	free(buf);
}

void printUserID(struct stat sb)
{
	ssize_t bufsiz;
	char * buf = NULL;
	int s = 0;
	struct passwd pwd;
	struct passwd *result;

	// borrowed some portions of code from the man pages
	bufsiz = sysconf(_SC_GETPW_R_SIZE_MAX);

	if (bufsiz == -1)          
		bufsiz = 16384;    

	buf = malloc(bufsiz);
	if (buf == NULL) 
	{
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	s = getpwuid_r(sb.st_uid, &pwd, buf, bufsiz, &result);
	if (result == NULL) 
	{
		if (s == 0)
			printf("Not found\n");
		else 
		{
			errno = s;
			perror("getpwnam_r");
		}
		exit(EXIT_FAILURE);
	}
	printf("  Owner Id:                 %s           (UID = %jd)\n", pwd.pw_name, (intmax_t) pwd.pw_uid);

	free(buf);
}

void printGrID(struct stat sb)
{
	int check = 0;
	struct group usr_grp;
	struct group *result;
	char * buf = NULL;
	ssize_t bufsiz;

	// portions of code were from the man pages.
	bufsiz = 16384;
	buf = malloc(bufsiz);
	if (buf == NULL)
	{
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	check = getgrgid_r(sb.st_gid, &usr_grp, buf, bufsiz, &result); 
	if (result == NULL)
	{
		if (check == 0)
			printf("Not found\n");
		else
		{
			perror("Something went wrong");
		}
		exit(EXIT_FAILURE);
	}
	printf("  Group Id:                 %s              (GID = %jd)\n", usr_grp.gr_name, (intmax_t) usr_grp.gr_gid);
	free(buf);
};
