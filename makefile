CC = gcc -g -Wall
PROG = mystat

all: $(PROG)

$(PROG): $(PROG).o
	$(CC) -o $(PROG) $^

$(PROG).o: $(PROG).c $(PROG).h
	$(CC) -c $^

clean cls:
	rm -f *.o $(PROG) *~ \#*
