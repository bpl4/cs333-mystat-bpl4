This C Programming project was about simulating "ls -l" linux command. It reads in a file from stdin and outputs
the file type, size, permission modes, etc. 

Usage:
./mystat <file_name1> <file_name2> ...

Example:
./mystat helloworld.txt helloworld.c 
