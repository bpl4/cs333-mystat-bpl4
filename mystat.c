#include "mystat.h"

int main(int argc, char * argv[])
{
	struct stat file_stat;

	// Read the files given to the command line
	if (argc < 2) // give error if no files given
	{
		fprintf(stderr, "Usage: %s <filename1> ... <filenameN>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if (lstat(argv[1], &file_stat) == -1) // likely redundant...
	{
		perror("lstat\n");
		exit(EXIT_FAILURE);
	}

	// for each argv, go through and print their stats.
	for (int i = 1; i < argc && lstat(argv[i], &file_stat) != -1; ++i)
	{
		printf("File: %s\n", argv[i]);
		
		// File type
		printf("  File type:                ");
		switch (file_stat.st_mode & S_IFMT)
		{
			case S_IFBLK:  
				printf("block device\n");            
				break;
			case S_IFCHR:  
				printf("character device\n");        
				break;
			case S_IFDIR:  
				printf("directory\n");               
				break;
			case S_IFIFO:  
				printf("FIFO/pipe\n");               
				break;
			case S_IFLNK:  
				printf("Symbolic link ");                 
				// print the file path that this file is linked to.
				printSymLink(argv[i]);
				break;
			case S_IFREG:  
				printf("regular file\n");            
				break;
			case S_IFSOCK: 
				printf("socket\n");                  
				break;
			default:       
				printf("unknown?\n");                
				break;
		}

		// Device ID
		printf("  Device ID Number:         %lxh/%lud\n", file_stat.st_dev, file_stat.st_dev);

		// INode number	
		printf("  I-node number:            %ju\n", (uintmax_t) file_stat.st_ino);

		// Mode
		printf("  Mode:                     ");
		printPerms(file_stat.st_mode);

		// Link Count
		printf("  Link count:               %ju\n", (uintmax_t) file_stat.st_nlink);
		
		// Owner ID
		printUserID(file_stat);

		// Group ID
		printGrID(file_stat);

		// Preferred I/O block size
		printf("  Preferred I/O block size: %jd bytes\n", (intmax_t) file_stat.st_blksize);

		// File Size
		printf("  File size:                %ju bytes\n", (uintmax_t) file_stat.st_size);

		// Blocks allocated
		printf("  Blocks allocated:         %jd\n", (intmax_t) file_stat.st_blocks);
		

		// Last file access

		// following the examples of the man pages.
		char outstr[100];
		struct tm *tmp;

		tmp = localtime(&file_stat.st_atim.tv_sec); 
		if (tmp == NULL)
		{
			perror("Unable to get local time");
			exit(EXIT_FAILURE);
		}

		if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S %z (%Z) %a (local)", tmp) == 0)
		{
			fprintf(stderr, "strftime call failed");
			exit(EXIT_FAILURE);
		}

		printf("  Last file access:         %s\n", outstr);

		// last file modification
		tmp = localtime(&file_stat.st_mtim.tv_sec);
		if (tmp == NULL)
		{
			perror("Unable to get local time");
			exit(EXIT_FAILURE);
		}

		if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S %z (%Z) %a (local)", tmp) == 0)
		{
			fprintf(stderr, "strftime call failed");
			exit(EXIT_FAILURE);
		}
		
		printf("  Last file modification:   %s\n", outstr);


		// Last status changed
		tmp = localtime(&file_stat.st_ctim.tv_sec);
		if (tmp == NULL)
		{
			perror("Unable to get local time");
			exit(EXIT_FAILURE);
		}

		if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S %z (%Z) %a (local)", tmp) == 0)
		{
			fprintf(stderr, "strftime call failed");
			exit(EXIT_FAILURE);
		}
		
		printf("  Last status change:       %s\n", outstr);

	}

	return EXIT_SUCCESS;
}
